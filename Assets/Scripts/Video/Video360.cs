using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Video360 : MonoBehaviour
{
    public VideoPlayer video;

    string urlVideo;

    private void Awake()
    {
        urlVideo = Application.streamingAssetsPath + "/" + "Ayutthaya.mp4";
        video.url = urlVideo;
        video.Prepare();
    }

    public void StartVideo()
    {
        video.Play();
    }
}
