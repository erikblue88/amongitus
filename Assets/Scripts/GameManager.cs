using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject _playerPrefab;
    [SerializeField] private Transform _spawnPoint;

    // Start is called before the first frame update
    void Start()
    {
        var newPlayer = PhotonNetwork.Instantiate(_playerPrefab.name, _spawnPoint.position, Quaternion.identity);
        if(PhotonNetwork.IsMasterClient)
        {
            newPlayer.GetComponent<PlayeController>().photonView.RPC("OnTagged", RpcTarget.AllBuffered);
        }
    }
}
