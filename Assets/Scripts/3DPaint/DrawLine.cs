using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class DrawLine : MonoBehaviour
{
    public GameObject linePrefab;
    public GameObject currentLine;
    public LineRenderer lineRenderer;
    //public EdgeCollider edgeCollider;
    public List<Vector3> fingerPositions;
    public Transform guia;

    private void Start()
    {
        CreateLine();
    }

    // Update is called once per frame
    void Update()
    {
        //if(Mouse.current.leftButton.wasPressedThisFrame)
        //{
        //    CreateLine();
        //}
        //if(Mouse.current.leftButton.isPressed)
        //{
        //Vector2 tempFingerPos = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
        Vector3 tempFingerPos = guia.position;
            if (Vector3.Distance(tempFingerPos, fingerPositions[fingerPositions.Count - 1]) > .1f)
            {
                UpdateLine(tempFingerPos);
            }
        //}
    }

    void CreateLine()
    {
        currentLine = Instantiate(linePrefab, Vector3.zero, Quaternion.identity);
        lineRenderer = currentLine.GetComponent<LineRenderer>();
        //edgeCollider = currentLine.GetComponent<EdgeCollider2D>();
        fingerPositions.Clear();
        //fingerPositions.Add(Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue()));
        //fingerPositions.Add(Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue()));}
        fingerPositions.Add(guia.position);
        fingerPositions.Add(guia.position);
        lineRenderer.SetPosition(0, fingerPositions[0]);
        lineRenderer.SetPosition(1, fingerPositions[1]);
        //edgeCollider.points = fingerPositions.ToArray();
    }
        
    void UpdateLine(Vector3 newFingerPos)
    {
        fingerPositions.Add(newFingerPos);
        lineRenderer.positionCount++;
        lineRenderer.SetPosition(lineRenderer.positionCount - 1, newFingerPos);
        //edgeCollider.points = fingerPositions.ToArray();
    }
}
