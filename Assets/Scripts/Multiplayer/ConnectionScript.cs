using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;

public class ConnectionScript : MonoBehaviourPunCallbacks
{
    [SerializeField] private TMP_Text _statusText;
    [SerializeField] private TMP_InputField _nameInput;
    [SerializeField] private Button _playButton;
    [SerializeField] private Button _joinButton;


    // Start is called before the first frame update
    void Start()
    {
        SetStatus(" ");
        SetInputEnabled(false);
        _nameInput.text = PlayerPrefs.GetString("playername");
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.ConnectUsingSettings();
    }

    private void SetInputEnabled(bool enabled)
    {
        _nameInput.interactable = enabled;
        _joinButton.interactable = enabled;
    }

    public override void OnConnectedToMaster()
    {
        SetStatus("Connected to master");
        SetInputEnabled(true);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        SetStatus($"Disconnected: {cause}");
    }

    public void OnClick_Join()
    {
        if (string.IsNullOrEmpty(_nameInput.text))
        {
            SetStatus("Please enter a name");
            return;
        }
        PhotonNetwork.NickName = _nameInput.text;
        PlayerPrefs.SetString("playername", _nameInput.text);
        PhotonNetwork.JoinRandomRoom();
    }

    public void OnClick_Play()
    {
        PhotonNetwork.LoadLevel(1);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        PhotonNetwork.CreateRoom("Occupath", new RoomOptions
        {
            MaxPlayers = 20
        });
    }

    public override void OnJoinedRoom()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            SetStatus("Join as master");
            _playButton.interactable = true;
        }
        else
        {
            SetStatus("Join as client");
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        SetStatus(newPlayer.NickName + "Has joined the room");
    }

    private void SetStatus(string message)
    {
        _statusText.text = message;
    }
}
