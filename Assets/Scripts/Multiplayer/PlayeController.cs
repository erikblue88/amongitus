using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

public class PlayeController : MonoBehaviourPunCallbacks, IPunObservable
{
    [SerializeField] private Camera _camera;
    [SerializeField] private TMP_Text _nameText;
    [SerializeField] private Color _taggedColour;
    [SerializeField] private float _touchbackDuration;

    private Color _initialColour;
    private bool _isTagged;
    private float _touchbackCountdown;
    private float _timeSpentTagged;

    private void Awake()
    {
        if(!photonView.IsMine)
        {
            Destroy(_camera.gameObject);
        }
        UpdateNameDisplay();
        _initialColour = GetComponentInChildren<SkinnedMeshRenderer>().material.color;
    }

    [PunRPC]
    public void OnTagged()
    {
        _isTagged = true;
        _touchbackCountdown = _touchbackDuration;
        GetComponentInChildren<SkinnedMeshRenderer>().material.color = _taggedColour;
    }

    [PunRPC]
    public void OnUntagged()
    {
        _isTagged = false;
        GetComponentInChildren<SkinnedMeshRenderer>().material.color = _initialColour;
    }

    private void OnCollisionEnter(Collision collision)
    {
        var otherPlayer = collision.collider.GetComponent<PlayeController>();
        if(otherPlayer != null)
        {
            if(_isTagged && _touchbackCountdown <= 0f)
            {
                photonView.RPC("OnUntagged", RpcTarget.AllBuffered);
                otherPlayer.photonView.RPC("OnTagged", RpcTarget.AllBuffered);
            }
        }
    }

    private void Update()
    {
        UpdateNameDisplay();
        if (photonView.IsMine)
        {
            if (_touchbackCountdown > 0f)
            {
                _touchbackCountdown -= Time.deltaTime;
            }
            if(_isTagged)
            {
                _timeSpentTagged += Time.deltaTime;
            }
        }
    }

    private void UpdateNameDisplay()
    {
        var isWinning = FindObjectsOfType<PlayeController>().OrderBy(p => p._timeSpentTagged).First() == this;

        if(isWinning)
        {
            _nameText.text = $"<color=green>{photonView.Owner.NickName}</color>\n<size=50%>{_timeSpentTagged:F1} sec</size>";
        }
        else
        {
            _nameText.text = $"{photonView.Owner.NickName}\n<size=50%>{_timeSpentTagged:F1} sec</size>";
        }
        
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)
        {
            //stream.SendNext(_isTagged);
            stream.SendNext(_timeSpentTagged);
        }
        if(stream.IsReading)
        {
            //_isTagged = (bool)stream.ReceiveNext();
            _timeSpentTagged = (float)stream.ReceiveNext();

            UpdateNameDisplay();
        }
    }
}
