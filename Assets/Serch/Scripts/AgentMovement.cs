using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SerchGames
{
    public class AgentMovement : MonoBehaviour
    {

        CharacterController controller;

        [SerializeField]
        private Animator animator;

        public float rotationSpeed, movementSpeed, gravity = 20;

        Vector3 movementVector = Vector3.zero;

        private float desiredRotationAngle = 0;

        private Vector3 input;

        private void Start()
        {
            controller = GetComponent<CharacterController>();

            animator.GetComponent<Animator>();
        }

        private void Update()
        {
            if(controller.isGrounded)
            {
                if(movementVector.magnitude > 0)
                {
                    var animationSpeedMultiplier = SetCorrectAnimation();
                    RotateAgent();
                    movementVector *= animationSpeedMultiplier;
                }
            }

            movementVector.y -= gravity;
            controller.Move(movementVector * Time.deltaTime);
        }

        public void HanldeMovement(Vector2 input)
        {
            this.input = input;

            if(controller.isGrounded)
            {
                movementVector = transform.forward * movementSpeed;
            }
        }

        public void HandleMovementDirection(Vector3 direction)
        {
            desiredRotationAngle = Vector3.Angle(transform.forward,direction);

            var crossProduct = Vector3.Cross(transform.forward,direction).y;

            if(crossProduct < 0)
            {
                desiredRotationAngle *= -1;
            }
        }

        private void RotateAgent()
        {
            if(desiredRotationAngle > 10 || desiredRotationAngle < -10)
            {
                transform.Rotate(Vector3.up * desiredRotationAngle * rotationSpeed * Time.deltaTime);
            }
        }

        private float SetCorrectAnimation()
        {

            float currentAnimationSpeed = animator.GetFloat("move");

            var valueY = input.y;
            var valueX = input.x;

            var finalValue = 0.0f;

            if (valueX >= valueY)
            {
                finalValue = valueX;
            }
            else if (valueY >= valueX)
            {
                finalValue = valueY;
            }
                
            animator.SetFloat("move", finalValue);
            

            return currentAnimationSpeed;
        }

    }

}

