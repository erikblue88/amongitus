﻿using System;
using UnityEngine;

namespace SerchGames
{
    public interface iInput
    {
        Action<Vector2> OnMovementInput { get; set; }
        Action<Vector3> OnMovementDirectionInput { get; set; }
    }
}