using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SerchGames
{
    public class AgentController : MonoBehaviour
    {
        iInput input;

        AgentMovement movement;

        private void OnEnable()
        {
            input = GetComponent<iInput>();

            movement = GetComponent<AgentMovement>();

            input.OnMovementDirectionInput += movement.HandleMovementDirection;

            input.OnMovementInput += movement.HanldeMovement;
        }

        private void OnDisable()
        {
            input.OnMovementDirectionInput -= movement.HandleMovementDirection;

            input.OnMovementInput -= movement.HanldeMovement;
        }
    }
}



