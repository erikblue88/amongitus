using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace SerchGames
{
    public class PlayerInput : MonoBehaviour, iInput
    {

        public Action<Vector2> OnMovementInput { get; set; }
        public Action<Vector3> OnMovementDirectionInput { get; set; }

        private Vector3 directionToMoveIn;

        private Vector3 cameraDirection;

        private void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void Update()
        {
            GetMovementInput();
            GetMovementDirection();

            //Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            //Debug.Log("Input: " + input +"- Direction: " + directionToMoveIn);
        }

        private void GetMovementInput()
        {
            Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));


            Debug.Log("Input: " + input);

            if (input.y > 0)
            {
                cameraDirection = Camera.main.transform.forward;
            }

            if (input.y < 0)
            {
                cameraDirection = -Camera.main.transform.forward;
            }

            if (input.x > 0)
            {
                cameraDirection = Camera.main.transform.right;
            }

            if (input.x < 0)
            {
                cameraDirection = -Camera.main.transform.right;
            }

            if (input.x > 0 && input.y > 0)
            {
                cameraDirection = Camera.main.transform.forward + Camera.main.transform.right;
            }

            if (input.x < 0 && input.y < 0)
            {
                cameraDirection = (-Camera.main.transform.forward) + (-Camera.main.transform.right);
            }

            if (input.x > 0 && input.y < 0)
            {
                cameraDirection = (-Camera.main.transform.forward) + Camera.main.transform.right;
            }

            if (input.x < 0 && input.y > 0)
            {
                cameraDirection = Camera.main.transform.forward + (-Camera.main.transform.right);
            }


            input.x = Mathf.Abs(input.x);
            input.y = Mathf.Abs(input.y);


            OnMovementInput?.Invoke(input);
        }

        private void GetMovementDirection()
        {
            //Debug.DrawRay(Camera.main.transform.position, cameraForewardDirection * 10, Color.green);

            directionToMoveIn = Vector3.Scale(cameraDirection, (Vector3.right + Vector3.forward));

            //Debug.DrawRay(Camera.main.transform.position, directionToMoveIn * 10, Color.blue);

            //Debug.Log("Camera Direction: " + Camera.main.transform.forward + " --ToMove: "+directionToMoveIn);

            OnMovementDirectionInput?.Invoke(directionToMoveIn);
        }
    }

}


