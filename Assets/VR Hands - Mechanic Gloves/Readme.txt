Thank you for Purchasing!

Included are:
	- 1 rigged left hand glove model
	- 1 rigged right hand glove model
	- 9 different glove skins

Bonus:
	- basic grabbing scripts
	- animation controllers
	- grabbing animation


Credit to VRGameDev for creating the included scripts and tutorial series that can be found here:
https://www.youtube.com/watch?v=Gq3K48FozlA
https://www.youtube.com/watch?v=qX8kabgz3wo&t
